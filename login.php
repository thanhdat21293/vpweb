<?php 
if(isset($_SESSION['dao'])) {
  header("Location: " . $SERVER . "/incentive");
}
?>
<div id="login">
  <div class="container">
    <form class="col s12" id="loginform" method="post">
      <h3>INCENTIVES</h3>
      <p>Đăng nhập tài khoản</p>
      <?php
        if(isset($_POST['submit'])) {
          $email = $_POST['email'];
          $dao = $_POST['dao'];
          $password = $_POST['password'];
          if ($email && $dao && $password){
            $sql = "SELECT * FROM dbo.users WHERE email='".$email."' AND dao='".$dao."'";
            
            $params = array();
            $options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
            $stmt = sqlsrv_query( $conn, $sql, $params, $options );

            $row_count = sqlsrv_num_rows( $stmt );
            if ($row_count) {
              $result = sqlsrv_fetch_array( $stmt );
              if ($result['password'] == md5($password)) {
                $_SESSION['dao'] = $result['dao'];
                $_SESSION['email'] = $result['email'];
                $_SESSION['role'] = $result['role'];
                header("Location: " . $SERVER);
                die();
              } else {
                echo "<div class='message error'>Mật khẩu không đúng</div>";
              }
            } else {
              echo "<div class='message error'>Email hoặc DAO không đúng</div>";
            }
          } else {
            echo "<div class='message error'>Các trường không được để trống</div>";
          }
        }
      ?>
      <div class="row mb-0">
        <div class="input-field col s12">
          <input id="dao" type="text" name="dao" value="<?php if(isset($_POST['dao'])) echo $_POST['dao']; else echo "5818"; ?>">
          <label for="dao">Dao</label>
        </div>
      </div>
      <div class="row mb-0">
        <div class="input-field col s12">
          <input id="email" type="email" name="email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; else echo "thuphuong@vpbank.com.vn"; ?>">
          <label for="email">Email</label>
        </div>
      </div>
      <div class="row mb-0">
        <div class="input-field col s12">
          <input id="password" type="password" name="password" value="12345678">
          <label for="password" class="active">Mật khẩu</label>
        </div>
      </div>
      <a href="<?php echo $SERVER; ?>/register">Chưa có tài khoản?</a>
      <div class="right-align submit">
        <button class="waves-effect waves-light btn round fix primary-bg" type="submit" name="submit">Đăng nhập</button>
      </div>
    </form>
  </div>
</div>