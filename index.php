<?php 
  include('connect.php');
  if(isset($_GET['json'])) {
    if ($_GET['json'] == 'search-tra-soat') {
      $cif = $acctno = $f1 = $selectApp = "";
      $where = [];
      if (isset($_GET['selectApp'])) {
        $selectApp = $_GET['selectApp'];
        array_push($where, "app='".$selectApp."'");
      }
      if (isset($_GET['cif'])) {
        $cif = $_GET['cif'];
        array_push($where, "cif='".$cif."'");
      }
      if (isset($_GET['acctno'])) {
        $acctno = $_GET['acctno'];
        array_push($where, "acctno='".$acctno."'");
      }
      if (isset($_GET['cardNo']) && $_GET['cardNo']) {
        if ($selectApp == 'The') {
          $cardNo = $_GET['cardNo'];
          array_push($where, "schemedesc='".$cardNo."'");
        }
      }
      if (isset($_GET['f1']) && $_GET['f1']) {
        $f1 = $_GET['f1'];
        array_push($where, "app_id_c='".$f1."'");
      }
      if ($where) {
        $where1 = "WHERE " . join(' AND ', $where);
      }
      $sql1 = "SELECT acctno, app_id_c, dao_sp, dao_pb, dao_rm, dao_cif, point_bk FROM rawdata_201802 " . $where1;
      // $sql1 = "SELECT top 10 acctno, app_id_c, dao_sp, dao_pb, dao_rm, dao_cif, point_bk FROM rawdata_201802 ";
      // echo $sql1;
      $stmt1 = sqlsrv_query( $conn, $sql1 );
      $arrJson = [];
      while ($result1 = sqlsrv_fetch_object( $stmt1 )) {
        array_push($arrJson, $result1);
      }
      print_r(json_encode($arrJson));
      sqlsrv_free_stmt( $stmt1);
    }
  } else {
?>
<!DOCTYPE html>
<html>
<?php 
  include('head.php');
  $page = "";
  if(isset($_GET['page'])){
    $page = $_GET['page'];
  }
?>
<body>
<nav class="green darken-3">
  <div class="nav-wrapper container" id="nav-mobile">
    <ul id="nav-mobile" class="left">
      <li><a class="<?php if($page == 'incentive') echo 'active'; ?>" href="<?php echo $SERVER; ?>/incentive">Incentive</a></li>
      <li><a class="<?php if($page == 'rawdata') echo 'active'; ?>" href="<?php echo $SERVER; ?>/rawdata">RawData</a></li>
      <li><a class="<?php if($page == 'rawbq') echo 'active'; ?>" href="<?php echo $SERVER; ?>/rawbq">Raw_BQ</a></li>
      <li><a class="<?php if($page == 'chatluong') echo 'active'; ?>" href="<?php echo $SERVER; ?>/chatluong">Chat Luong</a></li>
      <li><a class="<?php if($page == 'baohiem') echo 'active'; ?>" href="<?php echo $SERVER; ?>/baohiem">Bao Hiem</a></li>
      <li><a class="<?php if($page == 'trasoat') echo 'active'; ?>" href="<?php echo $SERVER; ?>/trasoat">Tra Soat</a></li>
    </ul>
    <ul class="right">
      <li><a href="<?php echo $SERVER; ?>/account" style="display: inline-block"><?php echo $_SESSION['email']; ?></a></li>
      <li><a href="<?php echo $SERVER; ?>/logout">Thoát</a></li>
    </ul>
  </div>
</nav>
<br />
<br />
<br />
<?php 
  switch($page) {
    case "incentive":
      include("pages/incentive.php");
      break;

    case "rawdata":
      include("pages/rawdata.php");
      break;

    case "rawbq":
      include("pages/rawbq.php");
      break;

    case "chatluong":
      include("pages/chatluong.php");
      break;

    case "trasoat":
      include("pages/trasoat.php");
      break;

    default:
      include("pages/home.php");
  }
?>
<!-- <script src="js/jquery-3.2.1.slim.min.js"></script> -->
<!-- <script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script> -->
</body>
</html>
<?php 
  }
?>