<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VPBank</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/materialize.min.css" >
  <link rel="stylesheet" type="text/css" href="css/style.css" />

  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript" src="js/custom.js"></script>
  <!-- <script src="main.js"></script> -->
  <script>
    $(document).ready(function() {
      $('.tablescroll').scroll(function(e) { //detect a scroll event on the tbody
        /*
        Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
        of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.    
        */
        var number = $(".tablescroll").scrollLeft();
        $('.tablescroll .fixedd').css("left", number); //fix the first column of tdbody
        if (number > 0) {
          $('.tablescroll .fixedd').attr('border', '1'); //fix the first column of tdbody
        } else {
          $('.tablescroll .fixedd').attr('border', '0'); //fix the first column of tdbody
        }
      });
    });
  </script>
</head>