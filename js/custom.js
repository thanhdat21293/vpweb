const SERVER = "http://localhost/vpweb"
jQuery(document).ready(function() {
  jQuery('#traSoatFilter').click(function() {
    jQuery('.message-tra-soat').html('')
    jQuery('.message-tra-soat').removeClass('show')
    let form = jQuery('#formTraSoat').serialize()
    let selectApp = jQuery('#selectApp').val()
    let cif = jQuery('input[name="cif"]').val()
    let acctno = jQuery('input[name="acctno"]').val()
    form = form + '&selectApp=' + selectApp
    let msgHtml = ''
    if (!selectApp) {
      msgHtml += 'Bạn chưa chọn loại sản phẩm tra soát <br>'
    }
    if (!cif || !cif.trim()) {
      msgHtml += 'Cif là bắt buộc <br>'
    }
    if (!acctno || !acctno.trim()) {
      msgHtml += 'Acctno là bắt buộc'
    } 
    if (msgHtml) {
      jQuery('.message-tra-soat').html(msgHtml)
      jQuery('.message-tra-soat').addClass('show')
    } else {
      jQuery('.trasoatloading').removeClass('hide')
      jQuery('.inputtrasoat').prop("disabled", true)
      $.ajax({
        url: SERVER,
        method: "GET",
        data: form
      }).done(function(data) {
        data = JSON.parse(data)
        let html = ''
        data.map(item => {
          html += '<tr>'
          html += '<td>' + item.acctno + '</td>'
          html += '<td>' + item.app_id_c + '</td>'
          html += '<td>' + item.dao_sp + '</td>'
          html += '<td>' + item.dao_pb + '</td>'
          html += '<td>' + item.dao_rm + '</td>'
          html += '<td>' + item.dao_cif + '</td>'
          html += '<td>' + item.point_bk + '</td>'
          html += '</tr>'
        })
        if (!html) {
          html = '<tr><td style="text-align: center;" colspan="7">Không tìm thấy</td></tr>'
        }
        jQuery('.search-tra-soat tbody').html(html)
        jQuery('.inputtrasoat').prop("disabled", false)
        jQuery('.trasoatloading').addClass('hide')
      });
    }
  })
})