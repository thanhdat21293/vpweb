<!DOCTYPE html>
<?php 
  include('connect-login.php');
?>
<html>
<?php 
  include('head.php');
?>
<body>
<?php 
$page = "";
if(isset($_GET['page'])){
  $page = $_GET['page'];
}
switch($page) {
  case "login":
    include("login.php");
    break;

  case "register":
    include("register.php");
    break;

  case "logout":
    include("logout.php");
    break;

  default:
    include("404.php");
}
?>
<!-- <script src="js/jquery-3.2.1.slim.min.js"></script> -->
<!-- <script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script> -->
</body>
</html>