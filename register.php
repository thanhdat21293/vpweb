<?php 
  header("Location: " . $SERVER . "/incentive");
?>
<div id="login">
  <div class="container">
    <form class="col s12" id="loginform" method="post">
      <h3>INCENTIVES</h3>
      <p>Đăng ký tài khoản</p>
      <?php
        // function generateRandomString($length = 10) {
        //   return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
        // }
        if(isset($_POST['submit'])) {
          $email = $_POST['email'];
          $dao = $_POST['dao'];
          $password = md5('12345678');

          if ($email && preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/", $email)){
            $sql = "SELECT * FROM dbo.users WHERE email='".$email."' AND dao='".$dao."'";
            
            $params = array();
            $options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
            $stmt = sqlsrv_query( $conn, $sql, $params, $options );

            $row_count = sqlsrv_num_rows( $stmt );
            if (!$row_count) {
              $_POST['email'] = '';
              $_POST['dao'] = '';
              $sql1 = "INSERT INTO dbo.users (dao, email, password, role, status) VALUES ('".$dao."', '".$email."', '".$password."', 'member', 'active')";
              $stmt1 = sqlsrv_query( $conn, $sql1, $params, $options );
              $row_count2 = sqlsrv_num_rows( $stmt1 );
              if ($row_count2 > 0) {
                echo "<div class='message success'>Bạn đã đăng ký thành công</div>";
              } else {
                echo "<div class='message error'>Có lỗi</div>";
              }
            } else {
              echo "<div class='message error'>Email và DAO đã tồn tại</div>";
            }
          } else {
            echo "<div class='message error'>Email hoặc DAO không hợp lệ</div>";
          }
        }
      ?>
      <div class="row mb-0">
        <div class="input-field col s12">
          <input id="dao" type="text" name="dao" value="<?php if(isset($_POST['dao'])) echo $_POST['dao']; ?>">
          <label for="dao">Dao</label>
        </div>
      </div>
      <div class="row mb-0">
        <div class="input-field col s12">
          <input id="email" type="email" name="email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>">
          <label for="email">Email</label>
        </div>
      </div>
      <a href="<?php echo $SERVER; ?>/login">Đã có tài khoản?</a>
      <div class="right-align submit">
        <button type="submit" name="submit" class="waves-effect waves-light btn round fix primary-bg">Đăng ký</a>
      </div>
    </form>
  </div>
</div>