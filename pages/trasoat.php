﻿<?php 
if(! isset($_SESSION['dao'])) {
  header("Location: " . $SERVER . "/login");
}
?>
<div class="container">
  <div class="row">
    <div id="incentive">
      <h3 class="i-title">TRA SOÁT</h3>
      <div class="rawdata-table" style="overflow:hidden">
        <table class="bordered highlight">
          <thead>
            <tr>
              <th width="100" class="xanh">DAO</th>
              <th width="250" class="xanh">Tên CBBH</th>
              <th class="xanh">Vị trí</th>
              <th class="xanh">Sản phẩm theo tra soát</th>
            </tr>
          </thead>

          <tbody>
            <?php 
              $sql = "SELECT TOP 1 * FROM Trasoat WHERE [Dao_tra_soat]='".$_SESSION['dao']."'";
              $stmt = sqlsrv_query( $conn, $sql );
              while( $result = sqlsrv_fetch_array( $stmt ) ) {
                ?>
                <tr>
                  <td><?php echo $result['Dao_tra_soat']; ?></td>
                  <td><?php echo $result['salename']; ?></td>
                  <td><?php echo $result['Position']; ?></td>
                  <td style="position: relative;">
                    <select class="form-control inputtrasoat" id="selectApp">
                      <option value="">Select theo app</option>
                      <option value="Thau chi">Thấu chi</option>
                      <option value="Huy dong moi">Huy động mới</option>
                      <option value="The">Thẻ</option>
                      <option value="Trai phieu">Trái phiếu</option>
                      <option value="Cho vay">Cho vay</option>
                      <option value="KH gold">KH gold</option>
                      <option value="Huy dong tang rong">Huy động tăng ròng</option>
                    </select>
                  </td>
                </tr>
                <?php 
              }
              sqlsrv_free_stmt( $stmt);
            ?>
          </tbody>
        </table>
        <div class="row" style="margin-top: 20px;">
        <form class="col s12" id="formTraSoat" style="position: relative; margin-bottom: 40px">
          <input type="hidden" name="json" value="search-tra-soat">
          <div class="row">
            <div class="input-field col s12">
              <input id="last_name" type="text" class="validate col s6 inputtrasoat" name="cif">
              <label for="last_name">Cif*</label>
            </div>
            <div class="input-field col s12">
              <input id="last_name" type="text" class="validate col s6 inputtrasoat" name="acctno">
              <label for="last_name">Acctno*</label>
            </div>
            <div class="input-field col s12">
              <input id="last_name" type="text" class="validate col s6 inputtrasoat" name="cardNo">
              <label for="last_name">Card no</label>
            </div>
            <div class="input-field col s12">
              <input id="last_name" type="text" class="validate col s6 inputtrasoat" name="f1">
              <label for="last_name">F1</label>
            </div>
          </div>
          <div class="message error message-tra-soat">
            <div>

            </div>
          </div>
          <button class="btn waves-effect waves-light inputtrasoat" id="traSoatFilter" type="button" name="action" style="background: #2E7D32 ">Lọc</button>
        </form>
        <br><br><br><br>
        <div style="position: relative; display: block; width: 100%; float:left;">
          <table class="search-tra-soat">
            <div class="no-result trasoatloading hide">
              <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div>
            <thead>
              <tr>
                <th class="xanh">Acctno</th>
                <th class="xanh">F1</th>
                <th class="xanh">dao_sp</th>
                <th class="xanh">dao_pb</th>
                <th class="xanh">dao_rm</th>
                <th class="xanh">dao_cif</th>
                <th class="xanh">point_bk</th>
              </tr>
              <tbody>
                <tr><td style="text-align: center;" colspan="7"></td></tr>
              </tbody>
            </thead>
          </table>
        </div>
      </div>
      </div>
    </div>  
  </div>
</div>