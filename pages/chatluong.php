﻿<?php 
if(! isset($_SESSION['dao'])) {
  header("Location: " . $SERVER . "/login");
}
?>
<div class="container">
  <div class="row">
    <div id="rawdata">
      <h3 class="i-title">CHI TIẾT TÍNH ĐIỂM CHẤT LƯỢNG TRONG THÁNG</h3>
      <div class="rawdata-table">
        <table class="bordered highlight">
          <thead>
            <tr>
              <th class="xanh">month</th>
              <th class="xanh">app</th>
              <th class="xanh">BRANCH_ID</th>
              <th class="xanh">dao_sp</th>
              <th class="xanh">dao_pb</th>
              <th class="xanh">DAO_CODE2</th>
              <th class="">DAO_RM</th>
              <th class="xanh">ACCATNO</th>
              <th class="xanh">ACNT_CONTRACT_ID</th>
              <th class="xanh">category</th>
              <th class="xanh">PRODUCT_GROUP</th>
              <th class="xanh">openning_date</th>
              <th class="xanh">MATDT</th>
              <th class="xanh">issue_date</th>
              <th class="xanh">CIF</th>
              <th class="xanh">CUSTOMER_NAME</th>
              <th class="xanh">CUS_OPEN_DATE</th>
              <th class="xanh">BI_CHANNEL</th>
              <th class="xanh">BAL_QD</th>
              <th class="xanh">he_so</th>
              <th class="xanh">BALANCE_INCENTIVE</th>
              <th class="xanh">NOTE_DETAILS</th>
              <th class="xanh">NOTE_DETAILS_RM</th>
              <th class="xanh">phat_sinh_ovd</th>
              <th class="xanh">avgbal_6</th>
              <th class="xanh">avgbal_5</th>
              <th class="xanh">avgbal_4</th>
              <th class="xanh">avgbal_3</th>
              <th class="xanh">avgbal_2</th>
              <th class="xanh">avgbal_1</th>
              <th class="xanh">avgbal</th>
              <th class="xanh">cc_active</th>
              <th class="xanh">cc_active_1</th>
              <th class="xanh">cc_active_2</th>
              <th class="xanh">cc_active_3</th>
              <th class="xanh">cc_active_4</th>
              <th class="xanh">cc_active_5</th>
              <th class="xanh">cc_active_6</th>
              <th class="xanh">quality_cc_phatsinh_giaodich_yn</th>
              <th class="xanh">username</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $sql = "SELECT * FROM quality_201802 WHERE DAO_sp='".$_SESSION['dao']."'";
              $stmt = sqlsrv_query( $conn, $sql );
              while( $result = sqlsrv_fetch_array( $stmt ) ) {
            ?>
                <tr>
                  <td><?php echo $result['month']; ?></td>
                  <td><?php echo $result['app']; ?></td>
                  <td><?php echo $result['BRANCH_id']; ?></td>
                  <td><?php echo $result['DAO_sp']; ?></td>
                  <td><?php echo $result['DAO_pb']; ?></td>
                  <td><?php echo $result['DAO_CODE2']; ?></td>
                  <td><?php echo $result['DAO_RM']; ?></td>
                  <td><?php echo $result['ACCTNO']; ?></td>
                  <td><?php echo $result['ACNT_CONTRACT_ID']; ?></td>
                  <td><?php echo $result['category']; ?></td>
                  <td><?php echo $result['PRODUCT_GROUP']; ?></td>
                  <td class="textright"><?php if ($result['openning_date']) echo date_format($result['openning_date'], 'd/m/Y'); ?></td>
                  <td class="textright"><?php if ($result['MATDT']) echo date_format($result['MATDT'], 'd/m/Y'); ?></td>
                  <td class="textright"><?php if ($result['issue_date']) echo date_format($result['issue_date'], 'd/m/Y'); ?></td>
                  <td><?php echo $result['CIF']; ?></td>
                  <td><?php echo $result['CUSTOMER_NAME']; ?></td>
                  <td class="textright"><?php if ($result['CUS_OPEN_DATE']) echo date_format($result['CUS_OPEN_DATE'], 'd/m/Y'); ?></td>
                  <td><?php echo $result['BI_CHANNEL']; ?></td>
                  <td class="textright"><?php echo $result['BAL_QD']; ?></td>
                  <td class="textright"><?php echo $result['heso']; ?></td>
                  <td class="textright"><?php echo $result['BALANCE_INCENTIVE']; ?></td>
                  <td><?php echo $result['NOTE_DETAILS']; ?></td>
                  <td><?php echo $result['NOTE_DETAILS_RM']; ?></td>
                  <td><?php echo $result['phat_sinh_ovd']; ?></td>
                  <td class="textright"><?php if ($result['avgbal_6']) echo number_format($result['avgbal_6'], 2, ',', '.'); ?></td>
                  <td class="textright"><?php if ($result['avgbal_5']) echo number_format($result['avgbal_5'], 2, ',', '.'); ?></td>
                  <td class="textright"><?php if ($result['avgbal_4']) echo number_format($result['avgbal_4'], 2, ',', '.'); ?></td>
                  <td class="textright"><?php if ($result['avgbal_3']) echo number_format($result['avgbal_3'], 2, ',', '.'); ?></td>
                  <td class="textright"><?php if ($result['avgbal_2']) echo number_format($result['avgbal_2'], 2, ',', '.'); ?></td>
                  <td class="textright"><?php if ($result['avgbal_1']) echo number_format($result['avgbal_1'], 2, ',', '.'); ?></td>
                  <td class="textright"><?php if ($result['avgbal']) echo number_format($result['avgbal'], 2, ',', '.'); ?></td>
                  <td><?php echo $result['cc_active']; ?></td>
                  <td><?php echo $result['cc_active_1']; ?></td>
                  <td><?php echo $result['cc_active_2']; ?></td>
                  <td><?php echo $result['cc_active_3']; ?></td>
                  <td><?php echo $result['cc_active_4']; ?></td>
                  <td><?php echo $result['cc_active_5']; ?></td>
                  <td><?php echo $result['cc_active_6']; ?></td>
                  <td><?php echo $result['quality_cc_phatsinh_giaodich_yn']; ?></td>
                  <td><?php echo $result['username']; ?></td>
                </tr>
            <?php 
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>  
  </div>
</div>