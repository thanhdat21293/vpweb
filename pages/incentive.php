﻿<?php 
if(! isset($_SESSION['dao'])) {
  header("Location: " . $SERVER . "/login");
}
?>
<div class="container">
  <div class="row">
    <div id="incentive">
      <h3 class="i-title">BÁO CÁO KẾT QUẢ LƯƠNG KINH DOANH THÁNG 11/2017</h3>
      <span class="i-subtitle">Đơn vị tính: Triệu VNĐ</span>
      <div class="tablescroll">
        <table class="bordered highlight">
          <thead>
            <tr>
              <th colspan="4" class="col-1-1 textleft fixedd bdr">THÔNG TIN CỦA CÁN BỘ NHÂN VIÊN BÁN HÀNG</th>
              <th colspan="10" class="col-1-1 textleft"></th>
              <th colspan="36" class="col-1-2 textleft">KẾT QUẢ KINH DOANH TRONG THÁNG CỦA CÁN BỘ BÁN HÀNG</th>
              <th colspan="26" class="col-1-3 textleft">ĐIỂM CÁC SẢN PHẨM QUY ĐỔI THEO QUY ĐỊNH</th>
              <th rowspan="5" class="col-1-4">ĐIỂM DOANH SỐ BÁN CHÍNH</th>
              <th rowspan="5" class="col-1-4">ĐIỂM DOANH SỐ BÁN BỔ SUNG</th>
              <th rowspan="5" class="col-1-4">ĐIỂM DOANH SỐ GIỚI THIỆU</th>
              <th colspan="2" class="col-1-4">ĐIỂM CONTEST</th>
              <th colspan="8" class="col-1-4">ĐIỂM CHẤT LƯỢNG</th>
              <th colspan="3" class="col-1-4">ĐIỂM QUY MÔ</th>
              <th rowspan="5" class="col-1-4">KPI_NHÓM ( theo quyết định 603, chỉ áp dụng cho 20 chi nhánh Vùng 1 và Vùng 10</th>
              <th rowspan="5" class="col-1-4">TỔNG ĐIỂM QUY ĐỔI ( chỉ sử dụng để ra đơn giá )</th>
              <th rowspan="5" class="col-1-4">TỔNG ĐIỂM QUY ĐỔI ( sử dụng để xếp hạng)</th>
              <th rowspan="5" class="col-1-4">ĐIỂM SÀN</th>
              <th rowspan="5" class="col-1-4">ĐIỂM HỖ TRỢ (ĐỐI VỚI CTV)</th>
              <th colspan="14" class="col-1-6 textleft">XÉT CÁC ĐIỀU KIỆN CẦN VÀ ĐỦ ĐỂ ĐƯỢC LƯƠNG KINH DOANH</th>
              <th rowspan="5" class="col-1-6">TỶ LỆ ĐẠT LƯƠNG KINH DOANH</th>
              <th colspan="8" class="col-1-7">LƯƠNG KINH DOANH ĐẠT ĐƯỢC</th>
              <th rowspan="5" class="col-1-8">Ngoại lệ</th>
            </tr>
            <tr class="tr-border">
                <th rowspan="4" class="col-2-1 fixedd scoll">DAO</th>
                <th rowspan="4" class="col-2-1 fixedd scoll">MÃ NHÂN VIÊN</th>
                <th rowspan="4" class="col-2-1 fixedd scoll">TÊN CBBH</th>
                <th rowspan="4" class="col-2-1 fixedd bdr scoll">VỊ TRÍ</th>
                <th rowspan="4" class="col-2-1 canh-fixedd">MÃ CHI NHÁNH</th>
                <th rowspan="4" class="col-2-1">TÊN CHI NHÁNH</th>
                <th rowspan="4" class="col-2-1">HUB</th>
                <th rowspan="4" class="col-2-1">VÙNG</th>
                <th rowspan="4" class="col-2-1">THÀNH PHỐ</th>
                <th rowspan="4" class="col-2-1">PHÂN LOẠI THÀNH PHỐ</th>
                <th rowspan="4" class="col-2-1">LOẠI HỢP ĐỒNG</th>
                <th rowspan="4" class="col-2-1">NGÀY BẮT ĐẦU</th>
                <th rowspan="4" class="col-2-1">NGÀY KẾT THÚC</th>
                <th rowspan="4" class="col-2-1">THÂM NIÊN</th>
                <th colspan="10" class="col-2-2">DOANH SỐ GIẢI NGÂN</th>
                <th colspan="2" class="col-2-2">THẤU CHI</th>
                <th colspan="5" class="col-2-2">THẺ TÍN DỤNG</th>
                <th colspan="2" class="col-2-2">THẺ GHI NỢ QUỐC TẾ</th>
                <th colspan="8" class="col-2-2">HUY ĐỘNG BQ TĂNG RÒNG</th>
                <th colspan="3" class="col-2-2">TÀI KHOẢN TRẢ LƯƠNG ACTIVE</th>
                <th class="col-2-2">CASA ACTIVE</th>
                <th class="col-2-2">GOLD CUST</th>
                <th colspan="2" class="col-2-2">PHÍ BẢO HIỂM</th>
                <th class="col-2-2">TRÁI PHIẾU</th>
                <th class="col-2-2">C.CHỈ QUỸ</th>
                <th colspan="7" class="col-2-3">DOANH SỐ GIẢI NGÂN</th>
                <th colspan="2" class="col-2-3">THẤU CHI</th>
                <th colspan="5" class="col-2-3">THẺ TÍN DỤNG</th>
                <th colspan="2" class="col-2-3">THẺ GHI NỢ QUỐC TẾ</th>
                <th colspan="3" class="col-2-3">HUY ĐỘNG BQ TĂNG RÒNG</th>
                <th class="col-2-3">PAYROLL</th>
                <th class="col-2-3">CASA ACTIVE</th>
                <th class="col-2-3">GOLD CUST</th>
                <th colspan="2" class="col-2-3">PHÍ BẢO HIỂM</th>
                <th class="col-2-3">TRÁI PHIẾU</th>
                <th class="col-2-3">C.CHỈ QUỸ</th>
                <th rowspan="4" class="col-2-4">ĐIỂM CONTEST (sử dụng để tính LKD và xếp hạng)</th>
                <th rowspan="4" class="col-2-4">" ĐIỂM CONTEST ( chỉ để sử dụng để xếp hạng, không sử dụng để tính LKD)</th>
                <th rowspan="4" class="col-2-4">CASA</th>
                <th rowspan="4" class="col-2-4">CHO VAY CÓ TSĐB</th>
                <th rowspan="4" class="col-2-4">UPL</th>
                <th rowspan="4" class="col-2-4">THẤU CHI</th>
                <th colspan="2" class="col-2-4">THẺ TÍN DỤNG</th>
                <th rowspan="4" class="col-2-4">THẺ GHI NỢ</th>
                <th rowspan="4" class="col-1-4">TỔNG ĐIỂM CHẤT LƯỢNG</th>
                <th rowspan="4" class="col-2-4">Quy mô Huy động tăng ròng</th>
                <th rowspan="4" class="col-2-4">Quy mô cho vay có TSĐB </th>
                <th rowspan="4" class="col-1-4">TỔNG ĐIỂM QUY MÔ </th>
                <th colspan="2" class="col-2-5">ĐIỀU KIỆN CẦN</th>
                <th colspan="12" class="col-2-5">ĐIỀU KIỆN ĐỦ</th>
                <th rowspan="4" class="col-2-6">Điểm cộng thăng tiến ( chỉ áp dụng cho Vùng 4)</th>
                <th rowspan="4" class="col-2-6">Điểm thưởng</th>
                <th rowspan="4" class="col-2-6">Xếp hạng từ điểm bán chính</th>
                <th rowspan="4" class="col-2-6">"Xếp hạng( chỉ sử dụng để quy ra đơn giá)</th>
                <th rowspan="4" class="col-2-6">Gỡ trần (Y/N)</th>
                <th rowspan="4" class="col-2-6">Đơn giá</th>
                <th rowspan="4" class="col-2-6">Lương kinh doanh (Trước thuế)</th>
                <th rowspan="4" class="col-2-6">Xếp hạng</th>
            </tr>
            <tr>
              <th rowspan="2" class="col-3-1">Mua nhà phố</th>
              <th rowspan="2" class="col-3-1">Mua nhà dự án</th>
              <th rowspan="2" class="col-3-1">Mua Ô tô</th>
              <th rowspan="2" class="col-3-1">Tiêu dùng thường</th>
              <th rowspan="2" class="col-3-1">Kinh doanh</th>
              <th rowspan="2" class="col-3-1">UPL cho KH Payroll</th>
              <th rowspan="2" class="col-3-1">UPL cho KH trường học/ bệnh viện/ high income</th>
              <th rowspan="2" class="col-3-1">UPL Small ticket</th>
              <th rowspan="2" class="col-3-1">UPL thông thường</th>
              <th rowspan="2" class="col-3-1">Vay sổ tiết kiệm</th>
              <th rowspan="2" class="col-3-1">Thấu chi không TSĐB</th>
              <th rowspan="2" class="col-3-1">Thấu chi có TSĐB</th>
              <th rowspan="2" class="col-3-1">MC2</th>
              <th rowspan="2" class="col-3-1">Titanium Step up</th>
              <th rowspan="2" class="col-3-1">Titanium Lady</th>
              <th rowspan="2" class="col-3-1">Platinum</th>
              <th rowspan="2" class="col-3-1">VNA</th>
              <th rowspan="2" class="col-3-1">Gold Debit</th>
              <th rowspan="2" class="col-3-1">Other Debit</th>
              <th colspan="6" class="col-3-1">KHÁCH HÀNG MASS THÔNG THƯỜNG</th>
              <th rowspan="2" class="col-3-1">BIG TD</th>
              <th rowspan="2" class="col-3-1">VVIP CASA</th>
              <th rowspan="2" class="col-3-1">1-<4 triệu</th>
              <th rowspan="2" class="col-3-1">4-10 triệu</th>
              <th rowspan="2" class="col-3-1">> 10 triệu</th>
              <th rowspan="2" class="col-3-1">CASA active</th>
              <th rowspan="2" class="col-3-1">KH Gold Club mới</th>
              <th rowspan="2" class="col-3-1">Bảo hiểm Nhân thọ</th>
              <th rowspan="2" class="col-3-1">Bảo hiểm khác</th>
              <th rowspan="2" class="col-3-1">Trái phiếu</th>
              <th rowspan="2" class="col-3-1">Chứng chỉ quỹ</th>
              <th rowspan="2" class="col-3-2">Mua nhà phố</th>
              <th rowspan="2" class="col-3-2">Mua nhà dự án</th>
              <th rowspan="2" class="col-3-2">Mua Ô tô</th>
              <th rowspan="2" class="col-3-2">Tiêu dùng thường</th>
              <th rowspan="2" class="col-3-2">Kinh doanh</th>
              <th rowspan="2" class="col-3-2">UPL thường</th>
              <th rowspan="2" class="col-3-2">Vay sổ tiết kiệm</th>
              <th rowspan="2" class="col-3-2">Thấu chi không TSĐB</th>
              <th rowspan="2" class="col-3-2">Thấu chi có TSĐB</th>
              <th rowspan="2" class="col-3-2">MC2</th>
              <th rowspan="2" class="col-3-2">Titanium Step up</th>
              <th rowspan="2" class="col-3-2">Titanium Lady</th>
              <th rowspan="2" class="col-3-2">Platinum</th>
              <th rowspan="2" class="col-3-2">VNA</th>
              <th rowspan="2" class="col-3-2">Gold Debit</th>
              <th rowspan="2" class="col-3-2">Other Debit</th>
              <th colspan="2" class="col-3-2">ĐIỂM TD & CASA (MIN -20)</th>
              <th rowspan="2" class="col-3-2">HUY ĐỘNG (MAX 500)</th>
              <th rowspan="2" class="col-3-2">Payroll active</th>
              <th rowspan="2" class="col-3-2">CASA active</th>
              <th rowspan="2" class="col-3-2">KH Gold Club mới</th>
              <th rowspan="2" class="col-3-2">Bảo hiểm Nhân thọ</th>
              <th rowspan="2" class="col-3-2">Bảo hiểm khác</th>
              <th rowspan="2" class="col-3-2">Trái phiếu</th>
              <th rowspan="2" class="col-3-2">Chứng chỉ quỹ</th>
              <th rowspan="3" class="col-3-3">Thẻ h.động trong 3 tháng và dùng I2B</th>
              <th rowspan="3" class="col-3-3">Đạt ngưỡng g.dịch BQ</th>
              <th rowspan="3" class="col-2-4">Điểm bán chính >= điểm sàn</th>
              <th rowspan="3" class="col-2-4">Cheating (DAO SP # DAO F1)</th>
              <th colspan="4" class="col-2-4">Điều kiện của PB</th>
              <th colspan="6" class="col-2-4">Điều kiện của RM</th>
              <th colspan="2" class="col-2-4">Điều kiện của PSE</th>
            </tr>
            <tr>
              <th class="col-4-1">CASA-VND</th>
              <th class="col-4-1">CASA-Ng.tệ</th>
              <th class="col-4-1">TD-VND-Ngắn hạn</th>
              <th class="col-4-1">TD-VND-Trung hạn</th>
              <th class="col-4-1">TD-VND-Dài hạn</th>
              <th class="col-4-1">TD-Ngoại tệ</th>
              <th class="col-4-1">TD</th>
              <th class="col-4-1">CASA</th>
              <th rowspan="2" class="col-4-1">PB không áp dụng đk CIF trên 2M</th>
              <th class="col-4-1">Điểm áp dụng cho điều kiện CIF trên 2M</th>
              <th class="col-4-1">Điểm không áp dụng cho CIF trên 2M</th>
              <th class="col-4-1">Thỏa mãn?</th>
              <th colspan="2" class="col-4-1">Điều kiện 1</th>
              <th colspan="3" class="col-4-1">Điều kiện 2</th>
              <th rowspan="2" class="col-4-2">Thỏa mãn? (Y/N)</th>
              <th class="col-4-1">Điểm</th>
              <th class="col-4-1">Thỏa mãn?</th>
            </tr>
            <tr class="col-5-1">
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th></th>
              <th></th>
              <th>(Triệu VNĐ)</th>
              <th>(Hợp đồng)</th>
              <th>(Hợp đồng)</th>
              <th>(Thẻ)</th>
              <th>(Thẻ)</th>
              <th>(Thẻ)</th>
              <th>(Thẻ)</th>
              <th>(Thẻ)</th>
              <th>(Thẻ)</th>
              <th>(Thẻ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Tài khoản)</th>
              <th>(Tài khoản)</th>
              <th>(Khách hàng)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Triệu VNĐ)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th>(Điểm)</th>
              <th class="bgwhiter">(Point)</th>
              <th class="bgwhiter">(Point)</th>
              <th class="bgwhiter">(Y/N)</th>
              <th class="bgwhiter">No.Cust Gold</th>
              <th class="bgwhiter">Đk1 t.mãn?</th>
              <th class="bgwhiter">No.VNA/Pla</th>
              <th class="bgwhiter">Phí BH đ.lập</th>
              <th class="bgwhiter">Đk2 t.mãn?</th>
              <th class="bgwhiter">(Point)</th>
              <th class="bgwhiter">(Y/N)</th>
            </tr>
          </thead>

          <tbody>
            <?php 
              $sql = "SELECT TOP 20 * FROM incentive_201802 WHERE DAO='".$_SESSION['dao']."'";
              $stmt = sqlsrv_query( $conn, $sql );
              while( $result = sqlsrv_fetch_array( $stmt ) ) {
                ?>
                <tr>
                  <td style="min-width: 50px;" class="fixedd scoll"><?php echo $result['dao']; ?></td>
                  <td style="min-width: 70px;" class="fixedd scoll"><?php echo $result['staff_id']; ?></td>
                  <td style="min-width: 170px;" class="fixedd scoll"><?php echo $result['sale_name']; ?></td>
                  <td style="min-width: 90px;" class="fixedd bdr scoll"><?php echo $result['POSITION']; ?></td>
                  <td style="min-width: 80px;" class="canh-fixedd"><?php echo $result['branch_id']; ?></td>
                  <td style="min-width: 150px;"><?php echo $result['branch_name']; ?></td>
                  <td style="min-width: 50px;"><?php echo $result['hub']; ?></td>
                  <td style="min-width: 50px;"><?php echo $result['zone_id']; ?></td>
                  <td style="min-width: 110px;"><?php echo $result['city']; ?></td>
                  <td style="min-width: 50px;"><?php echo $result['city_phan_loai']; ?></td>
                  <td style="min-width: 120px;"><?php echo $result['contract_type']; ?></td>
                  <td style="min-width: 90px;" class="textright">
                  <?php 
                    if ($result['date_sale_start'])
                      echo date_format($result['date_sale_start'], 'd/m/Y'); 
                  ?>
                  </td>
                  <td style="min-width: 90px;" class="textright">
                  <?php 
                    if ($result['date_sale_off'])
                      echo date_format($result['date_sale_off'], 'd/m/Y'); 
                  ?></td>
                  <td style="min-width: 80px;"><?php echo $result['tham_nien']; ?></td>

                  <!-- KẾT QUẢ KINH DOANH TRONG THÁNG CỦA CÁN BỘ BÁN HÀNG -->
                  <td class="textright" style="min-width: 90px;"><?php echo $result['vay_nha_pho_disb_bal'] != 0 ? $result['vay_nha_pho_disb_bal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['vay_nha_du_an_disb_bal'] != 0 ? $result['vay_nha_du_an_disb_bal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['al_disb_bal'] != 0 ? $result['al_disb_bal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['vay_tieu_dung_thuong_disb_bal'] != 0 ? $result['vay_tieu_dung_thuong_disb_bal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['hhb_disb_bal'] != 0 ? $result['hhb_disb_bal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['upl_disb_bal_nhom1'] != 0 ? $result['upl_disb_bal_nhom1'] : '-'; ?></td>
                  <td class="textright" style="min-width: 130px;"><?php echo $result['upl_disb_bal_nhom2'] != 0 ? $result['upl_disb_bal_nhom2'] : '-'; ?></td>
                  <td class="textright" style="min-width: 80px;"><?php echo $result['upl_disb_bal_nhom3'] != 0 ? $result['upl_disb_bal_nhom3'] : '-'; ?></td>
                  <td class="textright" style="min-width: 80px;"><?php echo $result['upl_disb_bal_nhom4'] != 0 ? $result['upl_disb_bal_nhom4'] : '-'; ?></td>
                  <td class="textright" style="min-width: 80px;"><?php echo $result['passbook_disb_bal'] != 0 ? $result['passbook_disb_bal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 80px;"><?php echo $result['thauchi_khong_tsdb_no'] != 0 ? $result['thauchi_khong_tsdb_no'] : '-'; ?></td>
                  <td class="textright" style="min-width: 80px;"><?php echo $result['thauchi_tsdb_no'] != 0 ? $result['thauchi_tsdb_no'] : '-'; ?></td>
                  <td class="textright" style="min-width: 80px;"><?php echo $result['cc_mc2'] != 0 ? $result['cc_mc2'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['cc_titanium_stepup'] != 0 ? $result['cc_titanium_stepup'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['cc_titanium_lady'] != 0 ? $result['cc_titanium_lady'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['cc_platinum'] != 0 ? $result['cc_platinum'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['cc_vna'] != 0 ? $result['cc_vna'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['golddebit_card'] != 0 ? $result['golddebit_card'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['other_goldcard'] != 0 ? $result['other_goldcard'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['mass_casa_vnd_avgbal'] != 0 ? $result['mass_casa_vnd_avgbal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['mass_casa_ngoaite_avgbal'] != 0 ? $result['mass_casa_ngoaite_avgbal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 120px;"><?php echo $result['mass_td_vnd_nganhan_duoi6thang_avgbal'] != 0 ? $result['mass_td_vnd_nganhan_avgbal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 120px;"><?php echo $result['mass_td_vnd_trunghan_avgbal'] != 0 ? $result['mass_td_vnd_trunghan_avgbal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 120px;"><?php echo $result['mass_td_vnd_daihan_avgbal'] != 0 ? $result['mass_td_vnd_daihan_avgbal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['mass_td_ngoaite_avgbal'] != 0 ? $result['mass_td_ngoaite_avgbal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['bigtd_avgbal'] != 0 ? $result['bigtd_avgbal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['vvipcasa_avgbal'] != 0 ? $result['vvipcasa_avgbal'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['payroll_no_tier1'] != 0 ? $result['payroll_no_tier1'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['payroll_no_tier2'] != 0 ? $result['payroll_no_tier2'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['payroll_no_tier3'] != 0 ? $result['payroll_no_tier3'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['casa_no'] != 0 ? $result['casa_no'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['goldclub_customer'] != 0 ? $result['goldclub_customer'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['insurance_fee_nhantho'] != 0 ? $result['insurance_fee_nhantho'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['insurance_fee_khac'] != 0 ? $result['insurance_fee_khac'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['traiphieu'] != 0 ? $result['traiphieu'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['ccq'] != 0 ? $result['ccq'] : '-'; ?></td>

                  <!-- ĐIỂM CÁC SẢN PHẨM QUY ĐỔI THEO QUY ĐỊNH -->
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_vay_mua_nha_pho'] != 0 ? $result['point_vay_mua_nha_pho'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_vay_mua_nha_du_an'] != 0 ? $result['point_vay_mua_nha_du_an'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_al'] != 0 ? $result['point_al'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_vay_tieu_dung_thuong'] != 0 ? $result['point_vay_tieu_dung_thuong'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_hhb'] != 0 ? $result['point_hhb'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_upl'] != 0 ? $result['point_upl'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_passbook'] != 0 ? $result['point_passbook'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_thauchi_khong_tsdb'] != 0 ? $result['point_thauchi_khong_tsdb'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_thauchi_tsdb'] != 0 ? $result['point_thauchi_tsdb'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_mc2'] != 0 ? $result['point_mc2'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_titanium_stepup'] != 0 ? $result['point_titanium_stepup'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_titanium_lady'] != 0 ? $result['point_titanium_lady'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_platinum'] != 0 ? $result['point_platinum'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_vna'] != 0 ? $result['point_vna'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_gold_debit'] != 0 ? $result['point_gold_debit'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_other_debit'] != 0 ? $result['point_other_debit'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_td_all'] != 0 ? $result['point_td_all'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_casa_all'] != 0 ? $result['point_casa_all'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_huydong_all'] != 0 ? $result['point_huydong_all'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_payroll'] != 0 ? $result['point_payroll'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_casa'] != 0 ? $result['point_casa'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_gold_customer'] != 0 ? $result['point_gold_customer'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_baohiem_nhantho'] != 0 ? $result['point_baohiem_nhantho'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_baohiem_khac'] != 0 ? $result['point_baohiem_khac'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_traiphieu'] != 0 ? $result['point_traiphieu'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_ccq'] != 0 ? $result['point_ccq'] : '-'; ?></td>

                  
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_banchinh'] != 0 ? $result['point_banchinh'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_bancheo'] != 0 ? $result['point_bancheo'] : '-'; ?></td>
                  <td class="textright" style="min-width: 100px;"><?php echo $result['point_gioithieu'] != 0 ? $result['point_gioithieu'] : '-'; ?></td>

                  <!-- Diem contest -->
                  <td class="textright" style="min-width: 80px;"><?php echo $result['point_contest'] != 0 ? $result['point_contest'] : '-'; ?></td>
                  <td class="textright" style="min-width: 130px;"><?php echo $result['point_contest_ko_tinh_thuong'] != 0 ? $result['point_contest_ko_tinh_thuong'] : '-'; ?></td>

                  <!-- Diem chat luong -->
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quality_point_casa_no'] != 0 ? $result['quality_point_casa_no'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quality_secured_loan'] != 0 ? $result['quality_secured_loan'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quality_point_upl_no'] != 0 ? $result['quality_point_upl_no'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quality_point_thauchi_no'] != 0 ? $result['quality_point_thauchi_no'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quality_cc_hoatdong'] != 0 ? $result['quality_cc_hoatdong'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quality_cc_giaodich'] != 0 ? $result['quality_cc_giaodich'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quality_point_debit_card'] != 0 ? $result['quality_point_debit_card'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_quality'] != 0 ? $result['point_quality'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quymo_point_TD'] != 0 ? $result['quymo_point_TD'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['quymo_point_loan_TSDB'] != 0 ? $result['quymo_point_loan_TSDB'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_quymo'] != 0 ? $result['point_quymo'] : '-'; ?></td>
                  <td class="textright" style="min-width: 140px;"><?php echo $result['KPI_nhom'] != 0 ? $result['KPI_nhom'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_quydoi'] != 0 ? $result['point_quydoi'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_quydoi_contest'] != 0 ? $result['point_quydoi_contest'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_san'] != 0 ? $result['point_san'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_hotro'] != 0 ? $result['point_hotro'] : '-'; ?></td>

                  <!-- XÉT CÁC ĐIỀU KIỆN CẦN VÀ ĐỦ ĐỂ ĐƯỢC LƯƠNG KINH DOANH -->
                  <td class="textcenter" style="min-width: 90px;"><?php echo $result['dkcan_point_banchinh']; ?></td> <!-- Yes/ No / NULL -->
                  <td class="textright" style="min-width: 90px;"><?php echo $result['cheating'] != 0 ? $result['cheating'] : '-'; ?></td>
                  <td class="textcenter" style="min-width: 110px;"><?php echo $result['pb_khong_apdung_cif_cu']; ?></td> <!-- Y / NULL -->
                  <td class="textright" style="min-width: 130px;"><?php echo $result['point_portfolio'] != 0 ? $result['point_portfolio'] : '-'; ?></td>
                  <td class="textright" style="min-width: 130px;"><?php echo $result['point_portfolio_new'] != 0 ? $result['point_portfolio_new'] : '-'; ?></td>
                  <td class="textcenter" style="min-width: 90px;"><?php echo $result['dkdu_portfolio']; ?></td> <!-- Yes/ No/NULL -->
                  <td class="textright" style="min-width: 90px;"><?php echo $result['gold_customer_no'] != 0 ? $result['gold_customer_no'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['dkdu_rm_1'] != 0 ? $result['dkdu_rm_1'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['so_the_phat_hanh'] != 0 ? $result['so_the_phat_hanh'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['insurance_fee_doclap'] != 0 ? $result['insurance_fee_doclap'] : '-'; ?></td>
                  <td class="textright" style="min-width: 90px;"><?php echo $result['dkdu_rm_2'] != 0 ? $result['dkdu_rm_2'] : '-'; ?></td>
                  <td class="textcenter" style="min-width: 90px;"><?php echo $result['dkdu_rm'];; ?></td> <!-- Yes/No/NULL -->
                  <td class="textright" style="min-width: 90px;"><?php echo $result['point_newcus'] != 0 ? $result['point_newcus'] : '-'; ?></td>
                  <td class="textcenter" style="min-width: 90px;"><?php echo $result['dkdu_newcus']; ?></td> <!-- Yes/No/NULL -->

                  <td class="textright color-red" style="min-width: 90px;"><b><?php echo $result['tyle_dat_lkd'] != 0 ? $result['tyle_dat_lkd'] : '-'; ?></b></td>

                  <!-- LƯƠNG KINH DOANH ĐẠT ĐƯỢC -->
                  <td class="textright" style="min-width: 100px;"><?php echo $result['cong_diem_thang_tien'] != 0 ? $result['cong_diem_thang_tien'] : '-'; ?></td> <!-- NULL -->
                  <td class="textcenter" style="min-width: 90px;"><b><?php echo $result['point_thuong'] != 0 ? $result['point_thuong'] : '-'; ?></b></td>
                  <td class="" style="min-width: 90px;"><?php echo $result['xephang_banchinh']; ?></td>
                  <td class="" style="min-width: 90px;"><?php echo $result['xephang_not_contest']; ?></td>
                  <td class="" style="min-width: 90px;"><?php echo $result['go_tran']; ?></td> <!-- Yes / No -->
                  <td class="textright" style="min-width: 90px;"><?php echo $result['don_gia'] != 0 ? $result['don_gia'] : '-'; ?></td>
                  <td class="textcenter bg1 color-red" style="min-width: 90px;"><b><?php echo $result['incentive'] != 0 ? $result['incentive'] : '-'; ?></b></td>
                  <td class="textcenter bg1 color-red" style="min-width: 90px;"><b><?php echo $result['xephang'] != 0 ? $result['xephang'] : '-'; ?></b></td>

                  <td class="textright" style="min-width: 90px;"><?php echo $result['ngoai_le'] != 0 ? $result['ngoai_le'] : '-'; ?></td>
                </tr>
                <?php 
              }
              sqlsrv_free_stmt( $stmt);
            ?>
          </tbody>
        </table>
      </div>
    </div>  
  </div>
</div>