﻿<?php 
if(! isset($_SESSION['dao'])) {
  header("Location: " . $SERVER . "/login");
}
?>
<div class="container">
  <div class="row">
    <div id="rawdata">
      <h3 class="i-title">SAO KÊ CÁC SẢN PHẨM CHO VAY, HUY ĐỘNG, THẺ,…(LỌC Ở CỘT APP) ĐỂ CBBH TỰ KIỂM TRA KẾT QUẢ</h3>
      <div class="rawdata-table">
        <table class="bordered highlight">
          <thead>
            <tr>
              <th class="xanh" style="min-width: 150px">app</th>
              <th class="xanh">bi_project</th>
              <th class="xanh">product_group</th>
              <th>ky_han</th>
              <th class="xanh">product_code</th>
              <th class="xanh">category</th>
              <th class="xanh">branch_id</th>
              <th class="">acctno</th>
              <th class="">dao_sp</th>
              <th class="xanh">dao_app</th>
              <th class="xanh">dao_pb</th>
              <th class="xanh">dao_cif</th>
              <th class="">dao_rm</th>
              <th class="xanh">dao_gioithieu</th>
              <th class="xanh">cif</th>
              <th class="xanh">customer_name</th>
              <th class="">cus_open_date</th>
              <th class="">openning_date</th>
              <th class="xanh">issue_date</th>
              <th class="xanh ">matdt</th>
              <th class="xanh">tcsign_date</th>
              <th class="xanh">channel</th>
              <th class="xanh ">refer_by</th>
              <th class="xanh">avgbal_qd</th>
              <th class="xanh">qvgbal_lm</th>
              <th>avgbal_net</th>
              <th class="">avgbal_net_final</th>
              <th class="xanh">avgbal_td_cif_cn</th>
              <th class="xanh">avgbal_casa_cif</th>
              <th class="xanh">closed_won</th>
              <th class="xanh">phan_loai_kh</th>
              <th class="xanh">phan_loai_kh_lm</th>
              <th class="xanh">csr_gioi_thieu</th>
              <th>phan_loai_kh_final</th>
              <th>note_details</th>
              <th class="xanh">notes</th>
              <th class="xanh">he_so</th>
              <th class="xanh">point</th>
              <th class="xanh">quyet_dinh</th>
              <th>note_details_bk</th>
              <th class="xanh">point_bk</th>
              <th class="xanh">note_kh_gold</th>
              <th>he_so_rm</th>
              <th>he_so_gioithieu</th>
              <th>note_details_rm</th>
              <th class="xanh">point_rn</th>
              <th>dao_bqtr_rm</th>
              <th class="xanh">username</th>
              <th class="xanh">username_rm</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $sql = "SELECT * FROM rawBQ_201802 WHERE dao_sp='".$_SESSION['dao']."'";
              $stmt = sqlsrv_query( $conn, $sql );
              while( $result = sqlsrv_fetch_array( $stmt ) ) {
            ?>
                <tr>
                  <td><?php echo $result['app']; ?></td>
                  <td><?php echo $result['bi_project']; ?></td>
                  <td><?php echo $result['product_group']; ?></td>
                  <td><?php echo $result['ky_han']; ?></td>
                  <td><?php echo $result['product_code']; ?></td>
                  <td><?php echo $result['category']; ?></td>
                  <td><?php echo $result['branch_id']; ?></td>
                  <td><?php echo $result['acctno']; ?></td>
                  <td><?php echo $result['dao_sp']; ?></td>
                  <td><?php echo $result['dao_app']; ?></td>
                  <td><?php echo $result['dao_pb']; ?></td>
                  <td><?php echo $result['dao_cif']; ?></td>
                  <td><?php echo $result['dao_rm']; ?></td>
                  <td><?php echo $result['dao_gioithieu']; ?></td>
                  <td><?php echo $result['cif']; ?></td>
                  <td><?php echo $result['customer_name']; ?></td>
                  <td class="textright"><?php if ($result['cus_open_date']) echo date_format($result['cus_open_date'], 'd/m/Y'); ?></td>
                  <td class="textright"><?php if ($result['openning_date'])echo date_format($result['openning_date'], 'd/m/Y'); ?></td>
                  <td class="textright"><?php if ($result['issue_date'])echo date_format($result['issue_date'], 'd/m/Y'); ?></td>
                  <td><?php if ($result['matdt']) echo date_format($result['matdt'], 'd/m/Y'); ?></td>
                  <td><?php if ($result['tcsign_date']) echo date_format($result['tcsign_date'], 'd/m/Y'); ?></td>
                  <td><?php echo $result['channel']; ?></td>
                  <td><?php echo $result['refer_by']; ?></td>
                  <td class="textright"><?php echo $result['avgbal_qd']; ?></td>
                  <td class="textright"><?php echo $result['avgbal_lm']; ?></td>
                  <td class="textright"><?php echo $result['avgbal_net']; ?></td>
                  <td class="textright"><?php echo $result['avgbal_net_final']; ?></td>
                  <td class="textright"><?php echo $result['avgbal_td_cif_cn']; ?></td>
                  <td class="textright"><?php echo $result['avgbal_casa_cif']; ?></td>
                  <td><?php echo $result['closed_won']; ?></td>
                  <td><?php echo $result['phan_loai_kh']; ?></td>
                  <td><?php echo $result['phan_loai_kh_lm']; ?></td>
                  <td><?php echo $result['csr_gioi_thieu']; ?></td>
                  <td><?php echo $result['phan_loai_kh_final']; ?></td>
                  <td><?php echo $result['note_details']; ?></td>
                  <td><?php echo $result['notes']; ?></td>
                  <td class="textright"><?php echo $result['he_so']; ?></td>
                  <td class="textright"><?php echo $result['point']; ?></td>
                  <td><?php echo $result['quyet_dinh']; ?></td>
                  <td><?php echo $result['note_details_bk']; ?></td>
                  <td class="textright"><?php echo $result['point_bk']; ?></td>
                  <td><?php echo $result['note_kh_gold']; ?></td>
                  <td class="textright"><?php echo $result['he_so_rm']; ?></td>
                  <td class="textright"><?php echo $result['he_so_gioithieu']; ?></td>
                  <td><?php echo $result['note_details_rm']; ?></td>
                  <td class="textright"><?php echo $result['point_rm']; ?></td>
                  <td><?php echo $result['dao_bqtr_rm']; ?></td>
                  <td><?php echo $result['username']; ?></td>
                  <td><?php echo $result['username_rm']; ?></td>
                </tr>
            <?php 
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>  
  </div>
</div>