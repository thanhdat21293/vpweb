﻿<?php 
if(! isset($_SESSION['dao'])) {
  header("Location: " . $SERVER . "/login");
}
?>
<div class="container">
  <div class="row">
    <div id="rawdata">
      <h3 class="i-title">SAO KÊ HUY ĐỘNG ĐỂ KIỂM TRA CHỈ TIÊU BÌNH QUÂN TĂNG RÒNG</h3>
      <div class="rawdata-table">
        <table class="bordered highlight">
          <thead>
            <tr>
              <th class="xanh" style="min-width: 150px">app</th>
              <th>bi_project</th>
              <th>product_group</th>
              <th>product_code</th>
              <th class="xanh">category</th>
              <th>branch_id</th>
              <th class="xanh">acctno</th>
              <th class="xanh">dao_sp</th>
              <th class="xanh">dao_app</th>
              <th class="xanh">dao_pb</th>
              <th class="xanh">dao_cif</th>
              <th class="xanh">dao_rm</th>
              <th class="xanh">dao_gioithieu</th>
              <th class="xanh">dao_online</th>
              <th class="xanh">app_id_c</th>
              <th class="xanh">row_id</th>
              <th class="xanh">Id_theo_tien_do</th>
              <th class="xanh">cif</th>
              <th class="xanh">customer_name</th>
              <th>cus_open_date</th>
              <th>openning_date</th>
              <th>issue_date</th>
              <th>matdt</th>
              <th>first_dist_date</th>
              <th>tcsign_date</th>
              <th>bal_qd</th>
              <th>approved_amt</th>
              <th>tenor</th>
              <th>accumulate_disb_amt</th>
              <th>limit_amt_50</th>
              <th>balance_ghi_nhan_luy_ke</th>
              <th>balance_limit</th>
              <th>balance_incentive</th>
              <th>ct_uudai_vay</th>
              <th>chuong_trinh_sp</th>
              <th>vpb_Ink_partner</th>
              <th>qua_cpc</th>
              <th>schemedesc</th>
              <th>campaign_code</th>
              <th>ct_uu_dai_lai_suat</th>
              <th>du_an_chien_luoc</th>
              <th>channel</th>
              <th>cis</th>
              <th>kh_ca_hn</th>
              <th>refer_by</th>
              <th>xet_dk</th>
              <th>cheating</th>
              <th>avgbal_qd</th>
              <th>qvgbal_lm</th>
              <th>avgbal_net</th>
              <th>avgbal_td_cif_cn</th>
              <th>avgbal_casa_cif</th>
              <th>closed_won</th>
              <th>phan_loai_kh</th>
              <th>phan_loai_kh_lm</th>
              <th>csr_gioi_thieu</th>
              <th>tinh_diem</th>
              <th>note_details</th>
              <th>notes</th>
              <th>he_so</th>
              <th>point</th>
              <th>quyet_dinh</th>
              <th>note_details_bk</th>
              <th>point_bk</th>
              <th>ngoaile</th>
              <th>note_kh_gold</th>
              <th>he_so_rm</th>
              <th>note_details_rm</th>
              <th>point_rn</th>
              <th>dao_bqtr_rm</th>
              <th>distinct_app</th>
              <th>he_so_gioithieu</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $sql = "SELECT * FROM rawdata_201802 WHERE dao_sp='".$_SESSION['dao']."'";
              $stmt = sqlsrv_query( $conn, $sql );
              while( $result = sqlsrv_fetch_array( $stmt ) ) {
            ?>
                <tr>
                  <td><?php echo $result['app']; ?></td>
                  <td><?php echo $result['bi_project']; ?></td>
                  <td><?php echo $result['product_group']; ?></td>
                  <td><?php echo $result['product_code']; ?></td>
                  <td><?php echo $result['category']; ?></td>
                  <td><?php echo $result['branch_id']; ?></td>
                  <td><?php echo $result['acctno']; ?></td>
                  <td><?php echo $result['dao_sp']; ?></td>
                  <td><?php echo $result['dao_app']; ?></td>
                  <td><?php echo $result['dao_pb']; ?></td>
                  <td><?php echo $result['dao_cif']; ?></td>
                  <td><?php echo $result['dao_rm']; ?></td>
                  <td><?php echo $result['dao_gioithieu']; ?></td>
                  <td><?php echo $result['dao_online']; ?></td>
                  <td><?php echo $result['app_id_c']; ?></td>
                  <td><?php echo $result['row_id']; ?></td>
                  <td><?php echo $result['ld_theo_tien_do']; ?></td>
                  <td><?php echo $result['cif']; ?></td>
                  <td><?php echo $result['customer_name']; ?></td>
                  <td class="textright"><?php if ($result['cus_open_date']) echo date_format($result['cus_open_date'], 'd/m/Y'); ?></td>
                  <td class="textright"><?php if ($result['openning_date'])echo date_format($result['openning_date'], 'd/m/Y'); ?></td>
                  <td class="textright"><?php if ($result['issue_date'])echo date_format($result['issue_date'], 'd/m/Y'); ?></td>
                  <td><?php if ($result['matdt'])echo date_format($result['matdt'], 'd/m/Y'); ?></td>
                  <td><?php if ($result['first_dist_date'])echo date_format($result['first_dist_date'], 'd/m/Y'); ?></td>
                  <td><?php if ($result['tcsign_date'])echo date_format($result['tcsign_date'], 'd/m/Y'); ?></td>
                  <td class="textright"><?php echo $result['bal_qd'] != 0 ? number_format($result['bal_qd'], 2) : '- ';?></td>
                  <td><?php echo $result['approved_amt']; ?></td>
                  <td><?php echo $result['tenor']; ?></td>
                  <td><?php echo $result['accumulate_disb_amt']; ?></td>
                  <td><?php echo $result['limit_amt_50']; ?></td>
                  <td><?php echo $result['balance_ghi_nhan_luy_ke']; ?></td>
                  <td><?php echo $result['balance_limit']; ?></td>
                  <td><?php echo $result['balance_incentive']; ?></td>
                  <td><?php echo $result['ct_uudai_vay']; ?></td>
                  <td><?php echo $result['chuong_trinh_sp']; ?></td>
                  <td><?php echo $result['vpb_lnk_partner']; ?></td>
                  <td><?php echo $result['qua_cpc']; ?></td>
                  <td><?php echo $result['schemedesc']; ?></td>
                  <td><?php echo $result['campaign_code']; ?></td>
                  <td><?php echo $result['ct_uu_dai_lai_suat']; ?></td>
                  <td><?php echo $result['du_an_chien_luoc']; ?></td>
                  <td><?php echo $result['channel']; ?></td>
                  <td><?php echo $result['cis']; ?></td>
                  <td><?php echo $result['kh_ca_hn']; ?></td>
                  <td><?php echo $result['refer_by']; ?></td>
                  <td><?php echo $result['xet_dk']; ?></td>
                  <td><?php echo $result['cheating']; ?></td>
                  <td><?php echo $result['avgbal_qd']; ?></td>
                  <td><?php echo $result['avgbal_lm']; ?></td>
                  <td><?php echo $result['avgbal_net']; ?></td>
                  <td><?php echo $result['avgbal_td_cif_cn']; ?></td>
                  <td><?php echo $result['avgbal_casa_cif']; ?></td>
                  <td><?php echo $result['closed_won']; ?></td>
                  <td><?php echo $result['phan_loai_kh']; ?></td>
                  <td><?php echo $result['phan_loai_kh_lm']; ?></td>
                  <td><?php echo $result['csr_gioi_thieu']; ?></td>
                  <td><?php echo $result['tinh_diem']; ?></td>
                  <td><?php echo $result['note_details']; ?></td>
                  <td><?php echo $result['notes']; ?></td>
                  <td class="textright"><?php echo $result['he_so']; ?></td>
                  <td class="textright"><?php echo $result['point']; ?></td>
                  <td><?php echo $result['quyet_dinh']; ?></td>
                  <td><?php echo $result['note_details_bk']; ?></td>
                  <td class="textright"><?php echo $result['point_bk']; ?></td>
                  <td><?php echo $result['ngoaile']; ?></td>
                  <td><?php echo $result['note_kh_gold']; ?></td>
                  <td class="textright"><?php echo $result['he_so_rm']; ?></td>
                  <td><?php echo $result['note_details_rm']; ?></td>
                  <td class="textright"><?php echo $result['point_rm']; ?></td>
                  <td><?php echo $result['dao_bqtr_rm']; ?></td>
                  <td><?php echo $result['distinct_app']; ?></td>
                  <td class="textright"><?php echo $result['he_so_gioithieu']; ?></td>
                </tr>
            <?php 
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>  
  </div>
</div>